const TypeInfo = @import("builtin").TypeInfo;

pub const Mat4 = struct {
    data: [4][4]f32,
};

pub fn Vector(comptime len: u32, comptime child: type) type {
    return @Type(TypeInfo{
        .Vector = .{
            .len = len,
            .child = child,
        },
    });
}

pub const v2 = Vector(2, f32);
pub const v3 = Vector(3, f32);
pub const v4 = Vector(4, f32);

// ----------------------------------------------------------------------------

pub inline fn v2FromF32(f: f32) v2 {
    return @splat(2, f);
}

pub inline fn v3FromF32(f: f32) v3 {
    return @splat(3, f);
}

// ----------------------------------------------------------------------------

pub inline fn dot(a: anytype, b: anytype) f32 {
    comptime if (@TypeOf(a) != @TypeOf(b)) {
        compileError("a and b are diferent types\n");
    };
    switch (@TypeOf(a)) {
        v2 => {
            const vec = a * b;
            const result = vec[0] + vec[1];
            return result;
        },
        v3 => {
            const vec = a * b;
            const result = vec[0] + vec[1] + vec[2];
            return result;
        },
        v4 => {
            const vec = a * b;
            const result = vec[0] + vec[1] + vec[2] + vec[3];
            return result;
        },
        else => {
            @compileError("Invalid Type\n");
        },
    }
}
// ----------------------------------------------------------------------------

// ----------------------------------------------------------------------------
pub inline fn cross(a: anytype, b: anytype) switch (@TypeOf(a)) {
    v2 => f32,
    v3 => v3,
} {
    comptime if (@TypeOf(a) != @TypeOf(b)) {
        compileError("a and b are diferent types\n");
    };
    switch (@TypeOf(a)) {
        v2 => {
            const result = a[0] * b[1] - a[1] * b[0];
            return result;
        },
        v3 => {
            const result = v3{
                a[1] * b[2] - b[1] * a[2],
                a[2] * b[0] - b[2] * a[0],
                a[0] * b[1] - b[0] * a[1],
            };
            return result;
        },
        else => {
            @compileError("Invalid Type\n");
        },
    }
}
// ----------------------------------------------------------------------------

// ----------------------------------------------------------------------------
pub inline fn length(vec: anytype) f32 {
    const result = @sqrt(dot(vec, vec));
    return result;
}

pub inline fn vDistance(va: anytype, vb: anytype) f32 {
    return @fabs(length(va - vb));
}
// ----------------------------------------------------------------------------

// ----------------------------------------------------------------------------
pub inline fn normalize(vec: anytype) @TypeOf(vec) {
    const len = length(vec);
    switch (@TypeOf(vec)) {
        v2 => {
            if (@fabs(len) < 0.0001) return @splat(2, @as(f32, 0.0));
            return vec / @splat(2, len);
        },
        v3 => {
            if (@fabs(len) < 0.0001) return @splat(3, @as(f32, 0.0));
            return vec / @splat(3, len);
        },
        v4 => {
            if (@fabs(len) < 0.0001) return @splat(4, @as(f32, 0.0));
            return vec / @splat(4, len);
        },
        else => {
            @compileError("Invalid Type\n");
        },
    }
}

// ----------------------------------------------------------------------------

pub inline fn flerp(fa: f32, fb: f32, t: f32) f32 {
    return (1 - t) * fa + t * fb;
}

pub fn lerp(a: anytype, b: anytype, t: f32) @TypeOf(a) {
    comptime if (@TypeOf(a) != @TypeOf(b)) {
        compileError("a and b are diferent types\n");
    };
    switch (@TypeOf(v)) {
        v2 => {
            return v2{
                fclamp(v[0], vmin[0], vmax[0]),
                fclamp(v[1], vmin[1], vmax[1]),
            };
        },
        v3 => {
            return v3{
                fclamp(v[0], vmin[0], vmax[0]),
                fclamp(v[1], vmin[1], vmax[1]),
                fclamp(v[2], vmin[2], vmax[2]),
            };
        },
        v4 => {
            return v4{
                fclamp(v[0], vmin[0], vmax[0]),
                fclamp(v[1], vmin[1], vmax[1]),
                fclamp(v[2], vmin[2], vmax[2]),
                fclamp(v[4], vmin[4], vmax[4]),
            };
        },
        else => {
            @compileError("Invalid type, must be v2 or v3");
        },
    }
}

// ----------------------------------------------------------------------------

pub fn fclamp(n: f32, min: f32, max: f32) f32 {
    if (n < min) return min;
    if (n > max) return max;
    return n;
}

pub fn vclamp(v: anytype, vmin: @TypeOf(v), vmax: @TypeOf(v)) @TypeOf(v) {
    switch (@TypeOf(v)) {
        v2 => {
            return v2{
                fclamp(v[0], vmin[0], vmax[0]),
                fclamp(v[1], vmin[1], vmax[1]),
            };
        },
        v3 => {
            return v3{
                fclamp(v[0], vmin[0], vmax[0]),
                fclamp(v[1], vmin[1], vmax[1]),
                fclamp(v[2], vmin[2], vmax[2]),
            };
        },
        else => {
            @compileError("Invalid type, must be v2 or v3");
        },
    }
}

pub fn vclampLength(v: anytype, lmin: f32, lmax: f32) @TypeOf(v) {
    switch (@TypeOf(v)) {
        v2 => return normalize(v) * v2FromF32(fclamp(length(v), lmin, lmax)),
        v3 => return normalize(v) * v3FromF32(fclamp(length(v), lmin, lmax)),
        else => @compileError("Invalid type, must be v2 or v3"),
    }
}

pub const Color = struct {
    r: f32, g: f32, b: f32, a: f32
};

pub inline fn color(r: f32, g: f32, b: f32, a: f32) Color {
    return Color{
        .r = r,
        .g = g,
        .b = b,
        .a = a,
    };
}

pub const Point = struct {
    x: i32,
    y: i32,
};
