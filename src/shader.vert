#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(location = 0) in vec2 inPosition;

layout(location = 0) out vec2 fragTexCoord;
layout(location = 1) out vec2 winSize;

layout(push_constant) uniform UniformBufferObject {
	vec3 pos;
	vec2 scale;
	vec4 crop;
	vec2 winSize;
	int useMask;
} ubo;

void main() {
	vec2 cord = vec2(ubo.crop.x, ubo.crop.y);
	cord.x = ubo.crop.x + ubo.crop.z * (gl_VertexIndex / 2);
	cord.y = ubo.crop.y + ubo.crop.w * float(gl_VertexIndex == 1 || gl_VertexIndex == 2);
	fragTexCoord = cord;

	gl_Position = vec4(inPosition, 0.0, 1.0) *
		mat4(
			ubo.scale.x, 0, 0, ubo.pos.x,
			0, ubo.scale.y, 0, ubo.pos.y,
			0, 0, 1,           ubo.pos.z,
			0, 0, 0, 1
		);
}
