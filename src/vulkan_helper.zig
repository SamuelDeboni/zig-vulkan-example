const builtin = @import("builtin");
pub const c = struct {
    pub usingnamespace @cImport({
        @cDefine("GLFW_INCLUDE_NONE", {});
        @cInclude("GLFW/glfw3.h");
    });

    pub extern "c" fn glfwGetInstanceProcAddress(instance: vk.Instance, procname: [*:0]const u8) vk.PfnVoidFunction;
    pub extern "c" fn glfwGetPhysicalDevicePresentationSupport(instance: vk.Instance, pdev: vk.PhysicalDevice, queuefamily: u32) c_int;
    pub extern "c" fn glfwCreateWindowSurface(instance: vk.Instance, window: *GLFWwindow, allocation_callbacks: ?*const vk.AllocationCallbacks, surface: *vk.SurfaceKHR) vk.Result;
};

pub const vk = @import("vk.zig");

pub const enable_validation_layers = true;
pub var validation_layers = [_][*:0]const u8{"VK_LAYER_KHRONOS_validation"};
pub const required_device_extensions = [_][]const u8{vk.extension_info.khr_swapchain.name};

pub const BaseDispatch = struct {
    vkCreateInstance: vk.PfnCreateInstance,
    usingnamespace vk.BaseWrapper(@This());
};

pub const InstanceDispatch = struct {
    vkDestroyInstance: vk.PfnDestroyInstance,
    vkCreateDevice: vk.PfnCreateDevice,
    vkDestroySurfaceKHR: vk.PfnDestroySurfaceKHR,
    vkEnumeratePhysicalDevices: vk.PfnEnumeratePhysicalDevices,
    vkGetPhysicalDeviceProperties: vk.PfnGetPhysicalDeviceProperties,
    vkGetPhysicalDeviceFormatProperties: vk.PfnGetPhysicalDeviceFormatProperties,
    vkEnumerateDeviceExtensionProperties: vk.PfnEnumerateDeviceExtensionProperties,
    vkGetPhysicalDeviceSurfaceFormatsKHR: vk.PfnGetPhysicalDeviceSurfaceFormatsKHR,
    vkGetPhysicalDeviceSurfacePresentModesKHR: vk.PfnGetPhysicalDeviceSurfacePresentModesKHR,
    vkGetPhysicalDeviceSurfaceCapabilitiesKHR: vk.PfnGetPhysicalDeviceSurfaceCapabilitiesKHR,
    vkGetPhysicalDeviceQueueFamilyProperties: vk.PfnGetPhysicalDeviceQueueFamilyProperties,
    vkGetPhysicalDeviceSurfaceSupportKHR: vk.PfnGetPhysicalDeviceSurfaceSupportKHR,
    vkGetPhysicalDeviceMemoryProperties: vk.PfnGetPhysicalDeviceMemoryProperties,
    vkGetDeviceProcAddr: vk.PfnGetDeviceProcAddr,
    usingnamespace vk.InstanceWrapper(@This());
};

pub const DeviceDispatch = struct {
    vkDestroyDevice: vk.PfnDestroyDevice,
    vkGetDeviceQueue: vk.PfnGetDeviceQueue,
    vkCreateSemaphore: vk.PfnCreateSemaphore,
    vkCreateFence: vk.PfnCreateFence,
    vkCreateImageView: vk.PfnCreateImageView,
    vkDestroyImageView: vk.PfnDestroyImageView,
    vkDestroySemaphore: vk.PfnDestroySemaphore,
    vkDestroyFence: vk.PfnDestroyFence,
    vkGetSwapchainImagesKHR: vk.PfnGetSwapchainImagesKHR,
    vkCreateSwapchainKHR: vk.PfnCreateSwapchainKHR,
    vkDestroySwapchainKHR: vk.PfnDestroySwapchainKHR,
    vkAcquireNextImageKHR: vk.PfnAcquireNextImageKHR,
    vkDeviceWaitIdle: vk.PfnDeviceWaitIdle,
    vkWaitForFences: vk.PfnWaitForFences,
    vkResetFences: vk.PfnResetFences,
    vkQueueSubmit: vk.PfnQueueSubmit,
    vkQueuePresentKHR: vk.PfnQueuePresentKHR,
    vkCreateCommandPool: vk.PfnCreateCommandPool,
    vkDestroyCommandPool: vk.PfnDestroyCommandPool,
    vkAllocateCommandBuffers: vk.PfnAllocateCommandBuffers,
    vkFreeCommandBuffers: vk.PfnFreeCommandBuffers,
    vkQueueWaitIdle: vk.PfnQueueWaitIdle,
    vkCreateShaderModule: vk.PfnCreateShaderModule,
    vkDestroyShaderModule: vk.PfnDestroyShaderModule,
    vkCreatePipelineLayout: vk.PfnCreatePipelineLayout,
    vkDestroyPipelineLayout: vk.PfnDestroyPipelineLayout,
    vkCreateRenderPass: vk.PfnCreateRenderPass,
    vkDestroyRenderPass: vk.PfnDestroyRenderPass,
    vkCreateGraphicsPipelines: vk.PfnCreateGraphicsPipelines,
    vkDestroyPipeline: vk.PfnDestroyPipeline,
    vkCreateFramebuffer: vk.PfnCreateFramebuffer,
    vkDestroyFramebuffer: vk.PfnDestroyFramebuffer,
    vkBeginCommandBuffer: vk.PfnBeginCommandBuffer,
    vkEndCommandBuffer: vk.PfnEndCommandBuffer,
    vkAllocateMemory: vk.PfnAllocateMemory,
    vkFreeMemory: vk.PfnFreeMemory,
    vkCreateBuffer: vk.PfnCreateBuffer,
    vkDestroyBuffer: vk.PfnDestroyBuffer,
    vkGetBufferMemoryRequirements: vk.PfnGetBufferMemoryRequirements,
    vkMapMemory: vk.PfnMapMemory,
    vkUnmapMemory: vk.PfnUnmapMemory,
    vkBindBufferMemory: vk.PfnBindBufferMemory,
    vkCmdBeginRenderPass: vk.PfnCmdBeginRenderPass,
    vkCmdEndRenderPass: vk.PfnCmdEndRenderPass,
    vkCmdBindPipeline: vk.PfnCmdBindPipeline,
    vkCmdDraw: vk.PfnCmdDraw,
    vkCmdDrawIndexed: vk.PfnCmdDrawIndexed,
    vkCmdSetViewport: vk.PfnCmdSetViewport,
    vkCmdSetScissor: vk.PfnCmdSetScissor,
    vkCmdBindVertexBuffers: vk.PfnCmdBindVertexBuffers,
    vkCmdBindIndexBuffer: vk.PfnCmdBindIndexBuffer,
    vkCmdCopyBuffer: vk.PfnCmdCopyBuffer,
    vkCmdClearColorImage: vk.PfnCmdClearColorImage,
    vkCreateDescriptorSetLayout: vk.PfnCreateDescriptorSetLayout,
    vkDestroyDescriptorSetLayout: vk.PfnDestroyDescriptorSetLayout,
    vkCreateDescriptorPool: vk.PfnCreateDescriptorPool,
    vkDestroyDescriptorPool: vk.PfnDestroyDescriptorPool,
    vkAllocateDescriptorSets: vk.PfnAllocateDescriptorSets,
    vkFreeDescriptorSets: vk.PfnFreeDescriptorSets,
    vkUpdateDescriptorSets: vk.PfnUpdateDescriptorSets,
    vkCmdBindDescriptorSets: vk.PfnCmdBindDescriptorSets,
    vkResetCommandBuffer: vk.PfnResetCommandBuffer,
    vkCmdPushConstants: vk.PfnCmdPushConstants,
    vkCreateImage: vk.PfnCreateImage,
    vkGetImageMemoryRequirements: vk.PfnGetImageMemoryRequirements,
    vkBindImageMemory: vk.PfnBindImageMemory,
    vkCmdPipelineBarrier: vk.PfnCmdPipelineBarrier,
    vkCmdCopyBufferToImage: vk.PfnCmdCopyBufferToImage,
    vkDestroyImage: vk.PfnDestroyImage,
    vkCreateSampler: vk.PfnCreateSampler,
    vkDestroySampler: vk.PfnDestroySampler,

    usingnamespace vk.DeviceWrapper(@This());
};

pub const Image = struct {
    width: u32,
    height: u32,
    raw: []const u8,
};

const TGAHeader = packed struct {
    id_lenth: u8,
    colour_map_type: u8,
    data_type_code: u8,
    color_map_origin: u16,
    color_map_length: u16,
    color_map_depth: u8,
    x_origin: u16,
    y_origin: u16,
    width: u16,
    height: u16,
    bits_per_pixel: u8,
    image_descriptor: u8,
};

const std = @import("std");

/// A simple function that loads a simple Runlength encoded RGBA TGA image
pub fn loadTGA(al: *std.mem.Allocator, path: []const u8) !Image {
    const file_data = try std.fs.cwd().readFileAlloc(al, path, 1024 * 1024 * 128);
    defer al.free(file_data);
    const header = @ptrCast(*TGAHeader, &file_data[0]);

    // Assert that the image is Runlength encoded RGB
    if (header.data_type_code != 10) {
        return error.InvalidTGAFormat;
    }

    if (header.bits_per_pixel != 32) {
        return error.InvalidBitsPerPixel;
    }

    var data = file_data[(@sizeOf(TGAHeader) + header.id_lenth)..][0..(file_data.len - 26)];

    var result = Image{
        .width = header.width,
        .height = header.height,
        .raw = undefined,
    };
    var result_data = try al.alloc(u8, header.width * header.height * 4);
    for (result_data) |*rd| rd.* = 0;
    errdefer al.free(result.raw);

    var index: usize = 0;
    var texture_index: usize = 0;
    outer_loop: while (index < data.len) {
        const pb = data[index];
        index += 1;
        const packet_len = pb & 0x7f;

        if ((pb & 0x80) == 0x00) { // raw packet
            var i: usize = 0;
            while (i <= packet_len) : (i += 1) {
                result_data[texture_index] = data[index + 2];
                result_data[texture_index + 1] = data[index + 1];
                result_data[texture_index + 2] = data[index];
                result_data[texture_index + 3] = data[index + 3];
                texture_index += 4;
                if (texture_index >= result_data.len - 3) break :outer_loop;
                index += 4;
            }
        } else { // rl packet
            var i: usize = 0;
            while (i <= packet_len) : (i += 1) {
                result_data[texture_index] = data[index + 2];
                result_data[texture_index + 1] = data[index + 1];
                result_data[texture_index + 2] = data[index];
                result_data[texture_index + 3] = data[index + 3];
                texture_index += 4;
                if (texture_index >= result_data.len - 3) break :outer_loop;
            }
            index += 4;
        }
    }

    result.raw = result_data;
    return result;
}
