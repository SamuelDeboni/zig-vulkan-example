#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(location = 0) out vec4 outColor;

layout(location = 0) in vec2 uvCord;
layout(location = 1) in vec2 winSize;

layout(set = 0, binding = 1) uniform sampler2D texSampler;

vec2 normalizeUv(sampler2D t, vec2 uv, float aaf) {
  	vec2 ruv = uv;
    vec2 res = textureSize(t, 0);
    ruv = ruv * res + 0.5;
    
    // tweak fractionnal value of the texture coordinate
    vec2 fl = floor(ruv);
    vec2 fr = fract(ruv);
    vec2 aa = fwidth(ruv) * aaf;
    fr = smoothstep( vec2(0.5,0.5) - aa, vec2(0.5,0.5) + aa, fr);
	ruv = (fl + fr - 0.5) / res;

    return ruv;
}

void main() {
	outColor = texture(texSampler, normalizeUv(texSampler, uvCord, 0.75));
}

