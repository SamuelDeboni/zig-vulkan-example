const std = @import("std");
const builtin = @import("builtin");
const Allocator = std.mem.Allocator;
usingnamespace @import("vulkan_helper.zig");
usingnamespace @import("math3d.zig");

var w_width: u32 = 800;
var w_height: u32 = 600;
const max_frames_in_flight = 5;

// TODO: Add this stuff to a graphics context struct
var vkb: BaseDispatch = undefined;
var vki: InstanceDispatch = undefined;
var vkd: DeviceDispatch = undefined;

var instance: vk.Instance = undefined;
var physical_device: vk.PhysicalDevice = undefined;
var device: vk.Device = undefined;
var surface: vk.SurfaceKHR = undefined;
var window: *c.GLFWwindow = undefined;
var graphics_queue: vk.Queue = undefined;
var present_queue: vk.Queue = undefined;

var swapchain: vk.SwapchainKHR = undefined;
var swapchain_images: []vk.Image = undefined;
var swapchain_format: vk.Format = undefined;
var swapchain_extent: vk.Extent2D = undefined;
var swapchain_image_views: []vk.ImageView = undefined;

var depth_image: VulkanImage = undefined;
var depth_image_view: vk.ImageView = undefined;

var render_pass: vk.RenderPass = undefined;
var descriptor_set_layout: vk.DescriptorSetLayout = undefined;
var texture_descriptor_set_layout: vk.DescriptorSetLayout = undefined;
var texture_mask_descriptor_set_layout: vk.DescriptorSetLayout = undefined;
var pipeline_layout: vk.PipelineLayout = undefined;
var graphics_pipeline: vk.Pipeline = undefined;

var swapchain_framebuffers: []vk.Framebuffer = undefined;

var command_pool: vk.CommandPool = undefined;
var temp_command_pool: vk.CommandPool = undefined;

var draw_cmd_buffer: vk.CommandBuffer = undefined;

var image_available_semaphores: [max_frames_in_flight]vk.Semaphore = undefined;
var render_finished_semaphores: [max_frames_in_flight]vk.Semaphore = undefined;
var in_flight_fences: [max_frames_in_flight]vk.Fence = undefined;
var images_in_flight: [max_frames_in_flight]vk.Fence = undefined;

var framebuffer_resized = false;

const max_texture_count: u32 = 11024;
var descriptor_pool: vk.DescriptorPool = undefined;
var descriptor_sets: []vk.DescriptorSet = undefined;

// ================================================

var potato_texture: VulkanImage = undefined;
var potato_image_view: vk.ImageView = undefined;
var potato2_texture: VulkanImage = undefined;
var potato2_image_view: vk.ImageView = undefined;

var texture_sampler: vk.Sampler = undefined;

// Vertices

const VBuffer = struct {
    handle: vk.Buffer = .null_handle,
    memory_handle: vk.DeviceMemory = .null_handle,
};

fn destroyVBuffer(dev: vk.Device, buf: VBuffer) void {
    vkd.destroyBuffer(dev, buf.handle, null);
    vkd.freeMemory(dev, buf.memory_handle, null);
}

const VulkanImage = struct {
    handle: vk.Image = .null_handle,
    memory_handle: vk.DeviceMemory = .null_handle,
};

fn destroyVulkanImage(dev: vk.Device, image: VulkanImage) void {
    vkd.destroyImage(dev, image.handle, null);
    vkd.freeMemory(dev, image.memory_handle, null);
}

var vertex_buffer: VBuffer = .{};
var index_buffer: VBuffer = .{};

const Vertex = struct {
    pos: [2]f32,

    const binding_description = vk.VertexInputBindingDescription{
        .binding = 0,
        .stride = @sizeOf(Vertex),
        .input_rate = .vertex,
    };

    const attribute_description = [_]vk.VertexInputAttributeDescription{
        .{
            .binding = 0,
            .location = 0,
            .format = .r32g32_sfloat,
            .offset = @byteOffsetOf(Vertex, "pos"),
        },
    };
};

const vertices = [_]Vertex{
    .{ .pos = .{ -0.5, 0.5 } },
    .{ .pos = .{ -0.5, -0.5 } },
    .{ .pos = .{ 0.5, -0.5 } },
    .{ .pos = .{ 0.5, 0.5 } },
};
const indices = [_]u16{ 0, 1, 2, 0, 2, 3 };

const UniformBufferObject = extern struct {
    pos: [3]f32 align(16) = [_]f32{ 0, 0, 0 },
    scale: [2]f32 align(8) = [_]f32{ 1.0, 1.0 },
    crop: [4]f32 align(16) = [_]f32{ 0.0, 0.0, 1.0, 1.0 },
    win_size: [2]f32 align(8) = [_]f32{ 800, 600 },
    use_mask: u32 align(4) = 0,
};

fn framebufferResizeCallback(win: ?*c.GLFWwindow, width: c_int, height: c_int) callconv(.C) void {
    framebuffer_resized = true;
}

pub fn main() anyerror!void {
    var gpa = std.heap.GeneralPurposeAllocator(.{}){};
    defer _ = gpa.deinit();

    // init
    if (c.glfwInit() == c.GLFW_FALSE) return error.GlfwFailedToInit;
    c.glfwWindowHint(c.GLFW_CLIENT_API, c.GLFW_NO_API);
    c.glfwWindowHint(c.GLFW_RESIZABLE, c.GLFW_TRUE);
    window = c.glfwCreateWindow(
        @intCast(c_int, w_width),
        @intCast(c_int, w_height),
        "vulkan test",
        null,
        null,
    ) orelse return error.NotAbleToCreateWindow;
    _ = c.glfwSetFramebufferSizeCallback(window, framebufferResizeCallback);

    try initVulkan(&gpa.allocator);

    // Create semaphores
    {
        var i: usize = 0;
        while (i < max_frames_in_flight) : (i += 1) {
            image_available_semaphores[i] = try vkd.createSemaphore(device, .{ .flags = .{} }, null);
            render_finished_semaphores[i] = try vkd.createSemaphore(device, .{ .flags = .{} }, null);
            in_flight_fences[i] = try vkd.createFence(device, .{ .flags = .{ .signaled_bit = true } }, null);
            images_in_flight[i] = .null_handle;
        }
    }

    // Main loop
    var delta: f64 = 0.0;
    var itime: f64 = c.glfwGetTime();
    var ltime: f64 = 0.0;
    var current_frame: usize = 0;
    while (c.glfwWindowShouldClose(window) == 0) {
        c.glfwPollEvents();

        ltime = itime;
        itime = c.glfwGetTime();
        delta = itime - ltime;
        const delta_time = @floatCast(f32, delta);

        // Begin Draw frame
        _ = try vkd.waitForFences(device, 1, @ptrCast([*]const vk.Fence, &in_flight_fences[current_frame]), vk.TRUE, std.math.maxInt(u64));
        const image_result = vkd.acquireNextImageKHR(
            device,
            swapchain,
            std.math.maxInt(u64),
            image_available_semaphores[current_frame],
            .null_handle,
        ) catch |err| switch (err) {
            error.OutOfDateKHR => {
                try recreateSwapChain(&gpa.allocator);
                continue;
            },
            else => unreachable,
        };

        const image_index = image_result.image_index;

        if (current_frame == 0) { // put fps counter on title
            var buf = [_]u8{0} ** 256;
            const s: []u8 = try std.fmt.bufPrint(
                &buf,
                "fps = {d:0.4} - delta = {d:0.4}, images = {}",
                .{ 1.0 / delta, delta, image_index },
            );
            c.glfwSetWindowTitle(window, @ptrCast([*c]const u8, s.ptr));
        }

        if (images_in_flight[image_index] != .null_handle) {
            _ = try vkd.waitForFences(device, 1, @ptrCast([*]const vk.Fence, &images_in_flight[image_index]), vk.TRUE, std.math.maxInt(u64));
        }
        images_in_flight[image_index] = in_flight_fences[current_frame];

        const clear_values = [_]vk.ClearValue{
            .{ .color = .{ .float_32 = .{ 0, 0, 0, 1 } } },
            .{ .depth_stencil = .{ .depth = 1.0, .stencil = 0 } },
        };

        // Start command buffer recording
        try vkd.resetCommandBuffer(draw_cmd_buffer, .{});
        try vkd.beginCommandBuffer(draw_cmd_buffer, .{
            .flags = .{},
            .p_inheritance_info = null,
        });
        vkd.cmdBeginRenderPass(draw_cmd_buffer, .{
            .render_pass = render_pass,
            .framebuffer = swapchain_framebuffers[image_index],
            .render_area = .{
                .offset = .{ .x = 0, .y = 0 },
                .extent = swapchain_extent,
            },
            .clear_value_count = @intCast(u32, clear_values.len),
            .p_clear_values = @ptrCast([*]const vk.ClearValue, &clear_values),
        }, .@"inline");

        updateAndRenderer(delta_time);

        vkd.cmdEndRenderPass(draw_cmd_buffer);
        try vkd.endCommandBuffer(draw_cmd_buffer);
        // End command buffer recording

        // present
        const wait_stage = vk.PipelineStageFlags{ .color_attachment_output_bit = true };
        const submit_info = vk.SubmitInfo{
            .wait_semaphore_count = 1,
            .p_wait_semaphores = @ptrCast([*]const vk.Semaphore, &image_available_semaphores[current_frame]),
            .p_wait_dst_stage_mask = @ptrCast([*]const vk.PipelineStageFlags, &wait_stage),
            .command_buffer_count = 1,
            .p_command_buffers = @ptrCast([*]const vk.CommandBuffer, &draw_cmd_buffer),
            .signal_semaphore_count = 1,
            .p_signal_semaphores = @ptrCast([*]const vk.Semaphore, &render_finished_semaphores[current_frame]),
        };

        try vkd.resetFences(device, 1, @ptrCast([*]const vk.Fence, &in_flight_fences[current_frame]));
        try vkd.queueSubmit(graphics_queue, 1, @ptrCast([*]const vk.SubmitInfo, &submit_info), in_flight_fences[current_frame]);

        const present_result = vkd.queuePresentKHR(present_queue, .{
            .wait_semaphore_count = 1,
            .p_wait_semaphores = @ptrCast([*]const vk.Semaphore, &render_finished_semaphores[current_frame]),
            .swapchain_count = 1,
            .p_swapchains = @ptrCast([*]vk.SwapchainKHR, &swapchain),
            .p_image_indices = @ptrCast([*]const u32, &image_index),
            .p_results = null,
        }) catch |err| switch (err) {
            error.OutOfDateKHR => {
                try recreateSwapChain(&gpa.allocator);
                continue;
            },
            else => unreachable,
        };

        if (present_result == .suboptimal_khr or framebuffer_resized) {
            framebuffer_resized = false;
            try recreateSwapChain(&gpa.allocator);
            continue;
        }

        try vkd.queueWaitIdle(present_queue);
        current_frame = @mod(current_frame + 1, max_frames_in_flight);
    }

    try cleanupVulkan(&gpa.allocator);
}

var counter: f32 = 0;
fn updateAndRenderer(delta_time: f32) void {
    counter += delta_time * 3;

    vkd.cmdBindPipeline(draw_cmd_buffer, .graphics, graphics_pipeline);
    const offset: vk.DeviceSize = 0;
    vkd.cmdBindVertexBuffers(
        draw_cmd_buffer,
        0,
        1,
        @ptrCast([*]vk.Buffer, &vertex_buffer.handle),
        @ptrCast([*]const vk.DeviceSize, &offset),
    );
    vkd.cmdBindIndexBuffer(draw_cmd_buffer, index_buffer.handle, 0, .uint16);

    const max_squares = 8;
    var index: u32 = 0;
    while (index < max_squares) : (index += 1) {
        var ubo = UniformBufferObject{};
        const soffset: f32 = (@intToFloat(f32, index) / max_squares) * 6.283;
        ubo.pos[0] = (@cos(counter * 0.5 + soffset)) * @sin(counter * 0.2) * 0.8;
        ubo.pos[1] = (@sin(counter * 0.5 + soffset)) * @cos(counter * 0.2) * 0.8;
        ubo.pos[2] = (@intToFloat(f32, index) / max_squares);
        ubo.scale[0] = (1 - @intToFloat(f32, index) / max_squares + 0.5) * 0.2;
        ubo.scale[1] = (1 - @intToFloat(f32, index) / max_squares + 0.5) * 0.2;
        ubo.win_size[0] = @intToFloat(f32, w_width);
        ubo.win_size[1] = @intToFloat(f32, w_height);

        vkd.cmdPushConstants(
            draw_cmd_buffer,
            pipeline_layout,
            .{ .vertex_bit = true },
            0,
            @sizeOf(UniformBufferObject),
            @ptrCast(*const c_void, &ubo),
        );

        vkd.cmdBindDescriptorSets(
            draw_cmd_buffer,
            .graphics,
            pipeline_layout,
            0,
            1,
            @ptrCast([*]const vk.DescriptorSet, &descriptor_sets[if (index % 2 == 0) 0 else 1]),
            0,
            undefined,
        );

        vkd.cmdDrawIndexed(draw_cmd_buffer, @intCast(u32, indices.len), 1, 0, 0, 0);
    }
}

// ============================================================================
// Init and clenup
// ============================================================================

fn initVulkan(al: *Allocator) !void {
    vkb = try BaseDispatch.load(c.glfwGetInstanceProcAddress);

    try createInstance(al);
    // create surface
    if (c.glfwCreateWindowSurface(instance, window, null, &surface) != .success) {
        return error.SurfaceInitFailed;
    }
    try pickPhysicalDevice(al);
    try deviceCreation();
    try createSwapchain(al);
    try createDescriptorSetLayout();
    try createRenderPass();

    try createGraphicsPipeline();

    try createCommandPool();
    try createDepthResources();
    try createFramebuffers(al);

    const image = try loadTGA(al, "potato.tga");
    defer al.free(image.raw);
    const image2 = try loadTGA(al, "potato_inverted.tga");
    defer al.free(image2.raw);

    potato_texture = try createTextureImage(al, image);
    potato_image_view = try createImageView(potato_texture.handle, .r8g8b8a8_srgb, .{ .color_bit = true });

    potato2_texture = try createTextureImage(al, image2);
    potato2_image_view = try createImageView(potato2_texture.handle, .r8g8b8a8_srgb, .{ .color_bit = true });

    texture_sampler = try createTextureSampler();
    try createDescriptorPool();
    try createTextureDescriptorSets(al);

    vertex_buffer = try createVertexBuffer();
    index_buffer = try createIndexBuffer();

    try createCommandBuffers();
}

fn cleanupVulkan(al: *Allocator) !void {
    for (render_finished_semaphores) |s| vkd.destroySemaphore(device, s, null);
    for (image_available_semaphores) |s| vkd.destroySemaphore(device, s, null);
    for (in_flight_fences) |f| vkd.destroyFence(device, f, null);

    vkd.destroyCommandPool(device, command_pool, null);
    vkd.destroyCommandPool(device, temp_command_pool, null);

    try cleanupSwapchain(al);

    vkd.destroyDescriptorPool(device, descriptor_pool, null);

    vkd.destroySampler(device, texture_sampler, null);

    vkd.destroyImageView(device, potato_image_view, null);
    destroyVulkanImage(device, potato_texture);
    vkd.destroyImageView(device, potato2_image_view, null);
    destroyVulkanImage(device, potato2_texture);

    vkd.destroyDescriptorSetLayout(device, descriptor_set_layout, null);
    vkd.destroyDescriptorSetLayout(device, texture_descriptor_set_layout, null);
    vkd.destroyDescriptorSetLayout(device, texture_mask_descriptor_set_layout, null);

    destroyVBuffer(device, vertex_buffer);
    destroyVBuffer(device, index_buffer);

    vkd.destroyDevice(device, null);

    vki.destroySurfaceKHR(instance, surface, null);
    vki.destroyInstance(instance, null);

    c.glfwDestroyWindow(window);
    c.glfwTerminate();

    al.free(descriptor_sets);
}

fn cleanupSwapchain(al: *Allocator) !void {
    vkd.destroyImageView(device, depth_image_view, null);
    destroyVulkanImage(device, depth_image);

    for (swapchain_framebuffers) |sfb| vkd.destroyFramebuffer(device, sfb, null);
    vkd.destroyPipeline(device, graphics_pipeline, null);
    vkd.destroyPipelineLayout(device, pipeline_layout, null);
    vkd.destroyRenderPass(device, render_pass, null);
    for (swapchain_image_views) |siv| vkd.destroyImageView(device, siv, null);
    vkd.destroySwapchainKHR(device, swapchain, null);

    // for (uniform_buffers) |ub| {
    //     vkd.destroyBuffer(device, ub.handle, null);
    //     vkd.freeMemory(device, ub.memory_handle, null);
    // }

    // al.free(command_buffers);
    al.free(swapchain_framebuffers);
    al.free(swapchain_images);
    al.free(swapchain_image_views);
}

// ============================================================================
// Image and Texture related functions
// ============================================================================

fn createTextureSampler() !vk.Sampler {
    const sampler_info = vk.SamplerCreateInfo{
        .flags = .{},
        .mag_filter = .linear,
        .min_filter = .linear,
        .address_mode_u = .repeat,
        .address_mode_v = .repeat,
        .address_mode_w = .repeat,
        .anisotropy_enable = vk.FALSE,
        .max_anisotropy = 4.0,
        .border_color = .int_opaque_black,
        .unnormalized_coordinates = vk.FALSE,
        .compare_enable = vk.FALSE,
        .compare_op = .always,
        .mipmap_mode = .linear,
        .mip_lod_bias = 0.0,
        .min_lod = 0.0,
        .max_lod = 0.0,
    };

    return try vkd.createSampler(device, sampler_info, null);
}

fn createTextureImage(al: *Allocator, image: Image) !VulkanImage {
    const staging_buffer = try createBuffer(
        image.raw.len,
        .{ .transfer_src_bit = true },
        .{ .host_visible_bit = true, .host_coherent_bit = true },
    );

    {
        var data = try vkd.mapMemory(device, staging_buffer.memory_handle, 0, image.raw.len, .{});
        @memcpy(@ptrCast([*]u8, data), image.raw.ptr, image.raw.len);
        vkd.unmapMemory(device, staging_buffer.memory_handle);
    }

    var result_image = try createImage(
        image.width,
        image.height,
        .r8g8b8a8_srgb,
        .optimal,
        .{ .transfer_dst_bit = true, .sampled_bit = true },
        .{ .device_local_bit = true },
    );

    try transitionImageLayout(
        result_image.handle,
        .r8g8b8a8_srgb,
        .@"undefined",
        .transfer_dst_optimal,
    );

    try copyBufferToImage(staging_buffer.handle, result_image.handle, image.width, image.height);

    try transitionImageLayout(
        result_image.handle,
        .r8g8b8a8_srgb,
        .transfer_dst_optimal,
        .shader_read_only_optimal,
    );

    vkd.destroyBuffer(device, staging_buffer.handle, null);
    vkd.freeMemory(device, staging_buffer.memory_handle, null);

    return result_image;
}

fn createImage(
    width: u32,
    height: u32,
    format: vk.Format,
    tiling: vk.ImageTiling,
    usage: vk.ImageUsageFlags,
    properties: vk.MemoryPropertyFlags,
) !VulkanImage {
    var result_image = VulkanImage{};
    const image_info = vk.ImageCreateInfo{
        .image_type = .@"2d",
        .extent = .{
            .width = width,
            .height = height,
            .depth = 1,
        },
        .mip_levels = 1,
        .array_layers = 1,
        .format = format,
        .tiling = tiling,
        .initial_layout = .@"undefined",
        .usage = usage,
        .sharing_mode = .exclusive,
        .samples = .{ .@"1_bit" = true },
        .flags = .{},
        .queue_family_index_count = 0,
        .p_queue_family_indices = undefined,
    };

    result_image.handle = try vkd.createImage(device, image_info, null);
    const memory_requirements = vkd.getImageMemoryRequirements(device, result_image.handle);

    result_image.memory_handle = try vkd.allocateMemory(device, .{
        .allocation_size = memory_requirements.size,
        .memory_type_index = try findMemoryType(memory_requirements.memory_type_bits, properties),
    }, null);

    try vkd.bindImageMemory(device, result_image.handle, result_image.memory_handle, 0);
    return result_image;
}

fn transitionImageLayout(
    image: vk.Image,
    format: vk.Format,
    old_layout: vk.ImageLayout,
    new_layout: vk.ImageLayout,
) !void {
    const command_buffer = try beginSingleTimeCommands();

    var barrier = vk.ImageMemoryBarrier{
        .old_layout = old_layout,
        .new_layout = new_layout,
        .src_queue_family_index = vk.QUEUE_FAMILY_IGNORED,
        .dst_queue_family_index = vk.QUEUE_FAMILY_IGNORED,
        .image = image,
        .subresource_range = .{
            .aspect_mask = .{ .color_bit = true },
            .base_mip_level = 0,
            .level_count = 1,
            .base_array_layer = 0,
            .layer_count = 1,
        },
        .src_access_mask = .{},
        .dst_access_mask = .{},
    };

    if (new_layout == .depth_stencil_attachment_optimal) {
        barrier.subresource_range.aspect_mask = .{ .depth_bit = true };

        const has_stencil = (format == .d32_sfloat_s8_uint or format == .d24_unorm_s8_uint);
        if (has_stencil) barrier.subresource_range.aspect_mask.stencil_bit = true;
    }

    var source_stage: vk.PipelineStageFlags = undefined;
    var dest_stage: vk.PipelineStageFlags = undefined;
    if (old_layout == .@"undefined" and new_layout == .transfer_dst_optimal) {
        barrier.src_access_mask = .{};
        barrier.dst_access_mask = .{ .transfer_write_bit = true };

        source_stage = .{ .top_of_pipe_bit = true };
        dest_stage = .{ .transfer_bit = true };
    } else if (old_layout == .transfer_dst_optimal and new_layout == .shader_read_only_optimal) {
        barrier.src_access_mask = .{ .transfer_write_bit = true };
        barrier.dst_access_mask = .{ .shader_read_bit = true };

        source_stage = .{ .transfer_bit = true };
        dest_stage = .{ .fragment_shader_bit = true };
    } else if (old_layout == .@"undefined" and new_layout == .depth_stencil_attachment_optimal) {
        barrier.src_access_mask = .{};
        barrier.dst_access_mask = .{
            .depth_stencil_attachment_read_bit = true,
            .depth_stencil_attachment_write_bit = true,
        };

        source_stage = .{ .top_of_pipe_bit = true };
        dest_stage = .{ .early_fragment_tests_bit = true };
    } else {
        return error.unsuportedLayoutTransition;
    }

    vkd.cmdPipelineBarrier(
        command_buffer,
        source_stage,
        dest_stage,
        .{},
        0,
        undefined,
        0,
        undefined,
        1,
        @ptrCast([*]const vk.ImageMemoryBarrier, &barrier),
    );

    try endSingleTimeCommands(command_buffer);
}

fn copyBufferToImage(buffer: vk.Buffer, image: vk.Image, width: u32, height: u32) !void {
    const command_buffer = try beginSingleTimeCommands();

    const region = vk.BufferImageCopy{
        .buffer_offset = 0,
        .buffer_row_length = 0,
        .buffer_image_height = 0,

        .image_subresource = .{
            .aspect_mask = .{ .color_bit = true },
            .mip_level = 0,
            .base_array_layer = 0,
            .layer_count = 1,
        },

        .image_offset = .{ .x = 0, .y = 0, .z = 0 },
        .image_extent = .{ .width = width, .height = height, .depth = 1 },
    };

    vkd.cmdCopyBufferToImage(
        command_buffer,
        buffer,
        image,
        .transfer_dst_optimal,
        1,
        @ptrCast([*]const vk.BufferImageCopy, &region),
    );

    try endSingleTimeCommands(command_buffer);
}

fn createImageView(
    image: vk.Image,
    format: vk.Format,
    aspect_flags: vk.ImageAspectFlags,
) !vk.ImageView {
    const view_info = vk.ImageViewCreateInfo{
        .flags = .{},
        .image = image,
        .view_type = .@"2d",
        .format = format,
        .subresource_range = .{
            .aspect_mask = aspect_flags,
            .base_mip_level = 0,
            .level_count = 1,
            .base_array_layer = 0,
            .layer_count = 1,
        },
        .components = .{
            .r = .identity,
            .g = .identity,
            .b = .identity,
            .a = .identity,
        },
    };

    const image_view = try vkd.createImageView(device, view_info, null);
    return image_view;
}

// ============================================================================
// Instance and devices
// ============================================================================

fn createInstance(al: *Allocator) !void {
    var glfw_extension_count: u32 = 0;
    const glfw_extensions = c.glfwGetRequiredInstanceExtensions(&glfw_extension_count);
    std.debug.print("{} glfw extention {}\n", .{ glfw_extension_count, @typeName(@TypeOf(glfw_extensions)) });
    {
        var i: usize = 0;
        while (i < glfw_extension_count) : (i += 1) {
            std.debug.print("  - {s}\n", .{glfw_extensions[i]});
        }
    }

    var extention_names = @ptrCast([*]const [*:0]const u8, glfw_extensions);

    const app_info = vk.ApplicationInfo{
        .p_application_name = "vulkan test",
        .application_version = vk.makeVersion(0, 0, 0),
        .p_engine_name = "No Engine",
        .engine_version = vk.makeVersion(1, 0, 0),
        .api_version = vk.API_VERSION_1_2,
    };

    instance = vkb.createInstance(vk.InstanceCreateInfo{
        .flags = .{},
        .p_application_info = &app_info,
        .enabled_extension_count = glfw_extension_count,
        .pp_enabled_extension_names = extention_names,
        .enabled_layer_count = @boolToInt(enable_validation_layers),
        .pp_enabled_layer_names = @ptrCast([*][*:0]const u8, &validation_layers[0]),
    }, null) catch |err| switch (err) {
        error.LayerNotPresent => try vkb.createInstance(vk.InstanceCreateInfo{
            .flags = .{},
            .p_application_info = &app_info,
            .enabled_extension_count = glfw_extension_count,
            .pp_enabled_extension_names = extention_names,
            .enabled_layer_count = 0,
            .pp_enabled_layer_names = undefined,
        }, null),
        else => unreachable,
    };

    vki = try InstanceDispatch.load(instance, c.glfwGetInstanceProcAddress);
    errdefer vki.destroyInstance(instance, null);
}

var graphics_family: u32 = undefined;
var present_family: u32 = undefined;

fn pickPhysicalDevice(al: *Allocator) !void {
    var device_count: u32 = undefined;
    _ = try vki.enumeratePhysicalDevices(instance, &device_count, null);

    if (device_count == 0) return error.noDeviceFound;

    const p_devices = try al.alloc(vk.PhysicalDevice, device_count);
    defer al.free(p_devices);

    _ = try vki.enumeratePhysicalDevices(instance, &device_count, p_devices.ptr);

    var device_properties: vk.PhysicalDeviceProperties = undefined;
    for (p_devices) |pdev| {
        device_properties = vki.getPhysicalDeviceProperties(pdev);
        std.debug.print(
            \\device info:
            \\driver ver : {}
            \\name: {}
            \\type: {}
            \\api ver: {}.{}.{}
            \\
        ,
            .{
                device_properties.driver_version,
                device_properties.device_name,
                device_properties.device_type,
                vk.versionMajor(device_properties.api_version),
                vk.versionMinor(device_properties.api_version),
                vk.versionPatch(device_properties.api_version),
            },
        );

        var gf: ?u32 = null;
        var pf: ?u32 = null;
        {
            var queue_family_count: u32 = 0;
            vki.getPhysicalDeviceQueueFamilyProperties(pdev, &queue_family_count, null);

            const family_prop = try al.alloc(vk.QueueFamilyProperties, queue_family_count);
            defer al.free(family_prop);

            vki.getPhysicalDeviceQueueFamilyProperties(pdev, &queue_family_count, family_prop.ptr);

            for (family_prop) |fp, i| {
                if (fp.queue_flags.graphics_bit) gf = @intCast(u32, i);

                var present_support = try vki.getPhysicalDeviceSurfaceSupportKHR(pdev, @intCast(u32, i), surface);
                if (present_support != 0) {
                    pf = @intCast(u32, i);
                    if (gf != null and pf.? == gf.?) break;
                }
            }
        }

        const has_swap_chain = querrySwap: { // check for swapchain capabilities
            var formats: [8]vk.SurfaceFormatKHR = undefined;
            var format_count: u32 = 0;
            _ = try vki.getPhysicalDeviceSurfaceFormatsKHR(pdev, surface, &format_count, null);
            if (format_count != 0) {
                _ = try vki.getPhysicalDeviceSurfaceFormatsKHR(pdev, surface, &format_count, formats[0..format_count].ptr);
            }

            var present_modes: [8]vk.PresentModeKHR = undefined;
            var present_mode_count: u32 = 0;
            _ = try vki.getPhysicalDeviceSurfacePresentModesKHR(pdev, surface, &present_mode_count, null);
            if (present_mode_count != 0) {
                _ = try vki.getPhysicalDeviceSurfacePresentModesKHR(
                    pdev,
                    surface,
                    &present_mode_count,
                    present_modes[0..present_mode_count].ptr,
                );
            }

            break :querrySwap (format_count > 0 and present_mode_count > 0);
        };

        if (has_swap_chain and gf != null and pf != null) {
            physical_device = pdev;
            graphics_family = gf.?;
            present_family = pf.?;

            std.debug.print("pf: {}\ngf: {}\n", .{ present_family, graphics_family });

            break;
        }
    }
}

// Device creation
fn deviceCreation() !void {
    const p_queue_priorities = [_]f32{1.0};
    const device_queue_info = [_]vk.DeviceQueueCreateInfo{
        .{
            .flags = .{},
            .queue_family_index = graphics_family,
            .queue_count = 1,
            .p_queue_priorities = @ptrCast([*]const f32, &p_queue_priorities[0]),
        },
        .{
            .flags = .{},
            .queue_family_index = present_family,
            .queue_count = 1,
            .p_queue_priorities = @ptrCast([*]const f32, &p_queue_priorities[0]),
        },
    };

    const queue_count: u32 = if (graphics_family == present_family) 1 else 2;
    device = try vki.createDevice(physical_device, vk.DeviceCreateInfo{
        .flags = .{},
        .queue_create_info_count = queue_count,
        .p_queue_create_infos = @ptrCast([*]const vk.DeviceQueueCreateInfo, &device_queue_info),
        .enabled_layer_count = 0,
        .pp_enabled_layer_names = undefined,
        .enabled_extension_count = required_device_extensions.len,
        .pp_enabled_extension_names = @ptrCast([*]const [*:0]const u8, &required_device_extensions),
        .p_enabled_features = null,
    }, null);
    vkd = try DeviceDispatch.load(device, vki.vkGetDeviceProcAddr);
    errdefer vkd.destroyDevice(device, null);

    graphics_queue = vkd.getDeviceQueue(device, graphics_family, 0);
    present_queue = vkd.getDeviceQueue(device, present_family, 0);
}

// ============================================================================
// Swapchain
// ============================================================================

// Create swapchain and images view
fn createSwapchain(al: *Allocator) !void {

    // create swapchain
    {
        // Chose best format, sRGB if it is avaliable
        const format = blk: {
            var format_count: u32 = undefined;
            var formats: [16]vk.SurfaceFormatKHR = undefined;
            _ = try vki.getPhysicalDeviceSurfaceFormatsKHR(physical_device, surface, &format_count, null);
            _ = try vki.getPhysicalDeviceSurfaceFormatsKHR(physical_device, surface, &format_count, formats[0..format_count].ptr);

            for (formats[0..format_count]) |format| {
                if (format.format == .b8g8r8a8_srgb and format.color_space == .srgb_nonlinear_khr)
                    break :blk format;
            }

            break :blk formats[0];
        };

        // Chose best format, mailbox if is avaliable, fifo otherwise
        const present_mode = blk: {
            var modes_count: u32 = undefined;
            var modes: [8]vk.PresentModeKHR = undefined;
            _ = try vki.getPhysicalDeviceSurfacePresentModesKHR(physical_device, surface, &modes_count, null);
            _ = try vki.getPhysicalDeviceSurfacePresentModesKHR(physical_device, surface, &modes_count, modes[0..modes_count].ptr);

            var best_mode = vk.PresentModeKHR.fifo_khr;
            for (modes[0..modes_count]) |mode| {
                if (mode == .mailbox_khr) {
                    break :blk mode;
                }
            }

            break :blk best_mode;
        };

        // chose swap extent
        const capabilities: vk.SurfaceCapabilitiesKHR = try vki.getPhysicalDeviceSurfaceCapabilitiesKHR(physical_device, surface);
        const extent = blk: {
            // std.debug.print("cap {}\n", .{capabilities});
            if (capabilities.current_extent.width != std.math.maxInt(u32)) {
                break :blk capabilities.current_extent;
            } else {
                {
                    var w: c_int = 0;
                    var h: c_int = 0;
                    c.glfwGetFramebufferSize(window, &w, &h);
                    w_width = @intCast(u32, w);
                    w_height = @intCast(u32, h);
                }

                var actual_extent = vk.Extent2D{ .width = w_width, .height = w_height };

                actual_extent.width = std.math.clamp(
                    actual_extent.width,
                    capabilities.min_image_extent.width,
                    capabilities.max_image_extent.width,
                );

                actual_extent.height = std.math.clamp(
                    actual_extent.height,
                    capabilities.min_image_extent.height,
                    capabilities.max_image_extent.height,
                );

                break :blk actual_extent;
            }
        };

        const image_count = blk: {
            if (capabilities.max_image_count == 0 or
                capabilities.max_image_count > capabilities.min_image_count + 1)
            {
                break :blk capabilities.min_image_count + 1;
            }
            break :blk capabilities.max_image_count;
        };

        const concurrent = graphics_family != present_family;
        const queue_family_indices = [_]u32{ graphics_family, present_family };

        swapchain = try vkd.createSwapchainKHR(device, vk.SwapchainCreateInfoKHR{
            .flags = .{},
            .surface = surface,
            .min_image_count = image_count,
            .image_format = format.format,
            .image_color_space = format.color_space,
            .image_extent = extent,
            .image_array_layers = 1,
            .image_usage = .{ .color_attachment_bit = true },
            .image_sharing_mode = if (concurrent) .concurrent else .exclusive,
            .queue_family_index_count = if (concurrent) 2 else 0,
            .p_queue_family_indices = if (concurrent) &queue_family_indices else &([_]u32{ 0, 0 }),
            .pre_transform = .{ .identity_bit_khr = true },
            .composite_alpha = .{ .opaque_bit_khr = true },
            .present_mode = present_mode,
            .clipped = vk.TRUE,
            .old_swapchain = .null_handle,
        }, null);
        errdefer vkd.destroySwapchainKHR(device, swapchain, null);

        swapchain_format = format.format;
        swapchain_extent = extent;
    }

    // create images
    {
        var image_count: u32 = 0;
        _ = try vkd.getSwapchainImagesKHR(device, swapchain, &image_count, null);
        swapchain_images = try al.alloc(vk.Image, image_count);
        _ = try vkd.getSwapchainImagesKHR(device, swapchain, &image_count, swapchain_images.ptr);

        // create image views
        swapchain_image_views = try al.alloc(vk.ImageView, swapchain_images.len);
        for (swapchain_images) |si, i| {
            swapchain_image_views[i] = try createImageView(
                swapchain_images[i],
                swapchain_format,
                .{ .color_bit = true },
            );
        }
    }
}

fn recreateSwapChain(al: *Allocator) !void {
    { // handle window minimization
        var w: c_int = 0;
        var h: c_int = 0;
        c.glfwGetFramebufferSize(window, &w, &h);
        while (w == 0 or h == 0) {
            c.glfwGetFramebufferSize(window, &w, &h);
            c.glfwWaitEvents();
        }
    }

    try vkd.deviceWaitIdle(device);

    try cleanupSwapchain(al);
    try createSwapchain(al);

    try createRenderPass();
    try createGraphicsPipeline();
    try createDepthResources();
    try createFramebuffers(al);
    try createCommandBuffers();
}

fn findSupportedFormat(
    cantidates: []const vk.Format,
    tiling: vk.ImageTiling,
    features: vk.FormatFeatureFlags,
) !vk.Format {
    for (cantidates) |format| {
        const props = vki.getPhysicalDeviceFormatProperties(physical_device, format);

        if (tiling == .linear and props.linear_tiling_features.contains(features)) {
            return format;
        } else if (tiling == .optimal and props.optimal_tiling_features.contains(features)) {
            return format;
        }
    }
    return error.failedToFindSupportedFormat;
}

fn findDepthFormat() !vk.Format {
    return try findSupportedFormat(
        ([_]vk.Format{ .d32_sfloat, .d32_sfloat_s8_uint, .d24_unorm_s8_uint })[0..],
        .optimal,
        .{ .depth_stencil_attachment_bit = true },
    );
}

fn createDepthResources() !void {
    const depth_format = try findDepthFormat();
    const has_stencil = (depth_format == .d32_sfloat_s8_uint or depth_format == .d24_unorm_s8_uint);

    depth_image = try createImage(
        swapchain_extent.width,
        swapchain_extent.height,
        depth_format,
        .optimal,
        .{ .depth_stencil_attachment_bit = true },
        .{ .device_local_bit = true },
    );
    depth_image_view = try createImageView(depth_image.handle, depth_format, .{ .depth_bit = true });

    try transitionImageLayout(depth_image.handle, depth_format, .@"undefined", .depth_stencil_attachment_optimal);
}

// ============================================================================
// Graphics Pipeline
// ============================================================================

fn createRenderPass() !void {
    const color_attachment = vk.AttachmentDescription{
        .flags = .{},
        .format = swapchain_format,
        .samples = .{ .@"1_bit" = true },
        .load_op = .clear,
        .store_op = .store,
        .stencil_load_op = .dont_care,
        .stencil_store_op = .dont_care,
        .initial_layout = .@"undefined",
        .final_layout = .present_src_khr,
    };

    const color_attachment_ref = vk.AttachmentReference{
        .attachment = 0,
        .layout = .color_attachment_optimal,
    };

    const depth_attachment = vk.AttachmentDescription{
        .flags = .{},
        .format = try findDepthFormat(),
        .samples = .{ .@"1_bit" = true },
        .load_op = .clear,
        .store_op = .dont_care,
        .stencil_load_op = .dont_care,
        .stencil_store_op = .dont_care,
        .initial_layout = .@"undefined",
        .final_layout = .depth_stencil_attachment_optimal,
    };

    const depth_attachment_ref = vk.AttachmentReference{
        .attachment = 1,
        .layout = .depth_stencil_attachment_optimal,
    };

    const subpass = vk.SubpassDescription{
        .flags = .{},
        .pipeline_bind_point = .graphics,
        .color_attachment_count = 1,
        .p_color_attachments = @ptrCast([*]const vk.AttachmentReference, &color_attachment_ref),
        .p_depth_stencil_attachment = &depth_attachment_ref,

        .input_attachment_count = 0,
        .p_input_attachments = undefined,
        .p_resolve_attachments = null,
        .preserve_attachment_count = 0,
        .p_preserve_attachments = undefined,
    };

    const dependency = vk.SubpassDependency{
        .dependency_flags = .{},
        .src_subpass = vk.SUBPASS_EXTERNAL,
        .dst_subpass = 0,
        .src_stage_mask = .{ .color_attachment_output_bit = true },
        .src_access_mask = .{},
        .dst_stage_mask = .{ .color_attachment_output_bit = true },
        .dst_access_mask = .{ .color_attachment_write_bit = true, .color_attachment_read_bit = true },
    };

    render_pass = try vkd.createRenderPass(device, .{
        .flags = .{},
        .attachment_count = 2,
        .p_attachments = @ptrCast([*]const vk.AttachmentDescription, &([_]vk.AttachmentDescription{
            color_attachment,
            depth_attachment,
        })),
        .subpass_count = 1,
        .p_subpasses = @ptrCast([*]const vk.SubpassDescription, &subpass),
        .dependency_count = 1,
        .p_dependencies = @ptrCast([*]const vk.SubpassDependency, &dependency),
    }, null);
}

fn createDescriptorSetLayout() !void {
    const layout_bindings = [_]vk.DescriptorSetLayoutBinding{
        .{ // UBO layout
            .binding = 0,
            .descriptor_type = .uniform_buffer,
            .descriptor_count = 1,
            .stage_flags = .{ .vertex_bit = true },
            .p_immutable_samplers = null,
        },
        .{ // sampler layout
            .binding = 1,
            .descriptor_count = 1,
            .descriptor_type = .combined_image_sampler,
            .p_immutable_samplers = null,
            .stage_flags = .{ .fragment_bit = true },
        },
        .{ // mask layout
            .binding = 2,
            .descriptor_count = 1,
            .descriptor_type = .combined_image_sampler,
            .p_immutable_samplers = null,
            .stage_flags = .{ .fragment_bit = true },
        },
    };

    descriptor_set_layout = try vkd.createDescriptorSetLayout(device, .{
        .flags = .{},
        .binding_count = 1,
        .p_bindings = @ptrCast([*]const vk.DescriptorSetLayoutBinding, &layout_bindings[0]),
    }, null);

    texture_descriptor_set_layout = try vkd.createDescriptorSetLayout(device, .{
        .flags = .{},
        .binding_count = 1,
        .p_bindings = @ptrCast([*]const vk.DescriptorSetLayoutBinding, &layout_bindings[1]),
    }, null);

    texture_mask_descriptor_set_layout = try vkd.createDescriptorSetLayout(device, .{
        .flags = .{},
        .binding_count = 1,
        .p_bindings = @ptrCast([*]const vk.DescriptorSetLayoutBinding, &layout_bindings[2]),
    }, null);
}

fn createGraphicsPipeline() !void {
    const vert_shader_source = @embedFile("../shaders/vert.spv");
    const frag_shader_source = @embedFile("../shaders/frag.spv");

    const vert_shader_module = try vkd.createShaderModule(device, .{
        .flags = .{},
        .code_size = vert_shader_source.len,
        .p_code = @ptrCast([*]const u32, vert_shader_source),
    }, null);
    defer vkd.destroyShaderModule(device, vert_shader_module, null);

    const frag_shader_module = try vkd.createShaderModule(device, .{
        .flags = .{},
        .code_size = frag_shader_source.len,
        .p_code = @ptrCast([*]const u32, frag_shader_source),
    }, null);
    defer vkd.destroyShaderModule(device, frag_shader_module, null);

    const vert_shader_stage_info = vk.PipelineShaderStageCreateInfo{
        .flags = .{},
        .stage = .{ .vertex_bit = true },
        .module = vert_shader_module,
        .p_name = "main",
        .p_specialization_info = null,
    };

    const frag_shader_stage_info = vk.PipelineShaderStageCreateInfo{
        .flags = .{},
        .stage = .{ .fragment_bit = true },
        .module = frag_shader_module,
        .p_name = "main",
        .p_specialization_info = null,
    };

    const shader_stages = [_]vk.PipelineShaderStageCreateInfo{ vert_shader_stage_info, frag_shader_stage_info };

    const vertex_input_info = vk.PipelineVertexInputStateCreateInfo{
        .flags = .{},
        .vertex_binding_description_count = 1,
        .p_vertex_binding_descriptions = @ptrCast([*]const vk.VertexInputBindingDescription, &Vertex.binding_description),
        .vertex_attribute_description_count = @intCast(u32, Vertex.attribute_description.len),
        .p_vertex_attribute_descriptions = @ptrCast([*]const vk.VertexInputAttributeDescription, &Vertex.attribute_description),
    };

    const input_assembly = vk.PipelineInputAssemblyStateCreateInfo{
        .flags = .{},
        .topology = .triangle_list,
        .primitive_restart_enable = vk.FALSE,
    };

    const viewport = vk.Viewport{
        .x = 0.0,
        .y = 0.0,
        .width = @intToFloat(f32, swapchain_extent.width),
        .height = @intToFloat(f32, swapchain_extent.height),
        .min_depth = 0.0,
        .max_depth = 1.0,
    };

    const scissor = vk.Rect2D{ .offset = .{ .x = 0, .y = 0 }, .extent = swapchain_extent };

    const viewport_state = vk.PipelineViewportStateCreateInfo{
        .flags = .{},
        .viewport_count = 1,
        .p_viewports = @ptrCast([*]const vk.Viewport, &viewport),
        .scissor_count = 1,
        .p_scissors = @ptrCast([*]const vk.Rect2D, &scissor),
    };

    const rasterizer = vk.PipelineRasterizationStateCreateInfo{
        .flags = .{},
        .depth_clamp_enable = vk.FALSE,
        .rasterizer_discard_enable = vk.FALSE,
        .polygon_mode = .fill,
        .line_width = 1.0,
        .cull_mode = .{ .back_bit = true },
        .front_face = .clockwise,
        .depth_bias_enable = vk.FALSE,
        .depth_bias_constant_factor = 0.0,
        .depth_bias_clamp = 0.0,
        .depth_bias_slope_factor = 0.0,
    };

    const multisampling = vk.PipelineMultisampleStateCreateInfo{
        .flags = .{},
        .sample_shading_enable = vk.FALSE,
        .rasterization_samples = .{ .@"1_bit" = true },
        .min_sample_shading = 1.0,
        .p_sample_mask = null,
        .alpha_to_coverage_enable = vk.TRUE,
        .alpha_to_one_enable = vk.FALSE,
    };

    const color_blend_attachment = vk.PipelineColorBlendAttachmentState{
        .color_write_mask = .{ .r_bit = true, .g_bit = true, .b_bit = true, .a_bit = true },
        .blend_enable = vk.TRUE,
        .src_color_blend_factor = .src_alpha,
        .dst_color_blend_factor = .one_minus_src_alpha,
        .color_blend_op = .add,
        .src_alpha_blend_factor = .one,
        .dst_alpha_blend_factor = .zero,
        .alpha_blend_op = .add,
    };

    const color_blending = vk.PipelineColorBlendStateCreateInfo{
        .flags = .{},
        .logic_op_enable = vk.FALSE,
        .logic_op = .copy,
        .attachment_count = 1,
        .p_attachments = @ptrCast([*]const vk.PipelineColorBlendAttachmentState, &color_blend_attachment),
        .blend_constants = [4]f32{ 0.0, 0.0, 0.0, 0.0 },
    };

    const push_constant_range = vk.PushConstantRange{
        .offset = 0,
        .size = @sizeOf(UniformBufferObject),
        .stage_flags = .{ .vertex_bit = true },
    };

    const depth_stencil = vk.PipelineDepthStencilStateCreateInfo{
        .flags = .{},
        .depth_test_enable = vk.TRUE,
        .depth_write_enable = vk.TRUE,
        .depth_compare_op = .less,
        .depth_bounds_test_enable = vk.FALSE,
        .min_depth_bounds = 0.0,
        .max_depth_bounds = 1.0,
        .stencil_test_enable = vk.FALSE,
        .front = undefined,
        .back = undefined,
    };

    const descriptor_set_layouts = [_]vk.DescriptorSetLayout{
        texture_descriptor_set_layout,
        texture_mask_descriptor_set_layout,
    };
    pipeline_layout = try vkd.createPipelineLayout(device, .{
        .flags = .{},
        .set_layout_count = descriptor_set_layouts.len,
        .p_set_layouts = @ptrCast([*]const vk.DescriptorSetLayout, &descriptor_set_layouts[0]),
        .push_constant_range_count = 1,
        .p_push_constant_ranges = @ptrCast([*]const vk.PushConstantRange, &push_constant_range),
    }, null);

    const create_info = vk.GraphicsPipelineCreateInfo{
        .flags = .{},
        .stage_count = 2,
        .p_stages = &shader_stages,
        .p_vertex_input_state = &vertex_input_info,
        .p_input_assembly_state = &input_assembly,
        .p_tessellation_state = null,
        .p_viewport_state = &viewport_state,
        .p_rasterization_state = &rasterizer,
        .p_multisample_state = &multisampling,
        .p_depth_stencil_state = &depth_stencil,
        .p_color_blend_state = &color_blending,
        .p_dynamic_state = null,
        .layout = pipeline_layout,
        .render_pass = render_pass,
        .subpass = 0,
        .base_pipeline_handle = .null_handle,
        .base_pipeline_index = -1,
    };

    _ = try vkd.createGraphicsPipelines(
        device,
        .null_handle,
        1,
        @ptrCast([*]const vk.GraphicsPipelineCreateInfo, &create_info),
        null,
        @ptrCast([*]vk.Pipeline, &graphics_pipeline),
    );
}

fn createCommandPool() !void {
    command_pool = try vkd.createCommandPool(device, .{
        .flags = .{ .reset_command_buffer_bit = true },
        .queue_family_index = graphics_family,
    }, null);

    temp_command_pool = try vkd.createCommandPool(device, .{
        .flags = .{ .reset_command_buffer_bit = true, .transient_bit = true },
        .queue_family_index = graphics_family,
    }, null);
}

fn findMemoryType(type_filter: u32, flags: vk.MemoryPropertyFlags) !u32 {
    const mem_properties = vki.getPhysicalDeviceMemoryProperties(physical_device);

    var i: u32 = 0;
    var memory_type = while (i < mem_properties.memory_type_count) : (i += 1) {
        if (type_filter & (@as(u32, 1) << @truncate(u5, i)) != 0 and
            mem_properties.memory_types[i].property_flags.contains(flags))
        {
            break i;
        }
    } else {
        return error.FailedToFindSuitableMemoryType;
    };

    return memory_type;
}

fn createFramebuffers(al: *Allocator) !void {
    swapchain_framebuffers = try al.alloc(vk.Framebuffer, swapchain_image_views.len);

    // Create one framebuffer per image view
    for (swapchain_image_views) |iv, i| {
        const attachments = [_]vk.ImageView{ iv, depth_image_view };
        swapchain_framebuffers[i] = try vkd.createFramebuffer(device, .{
            .flags = .{},
            .render_pass = render_pass,
            .attachment_count = @intCast(u32, attachments.len),
            .p_attachments = @ptrCast([*]const vk.ImageView, &attachments),
            .width = swapchain_extent.width,
            .height = swapchain_extent.height,
            .layers = 1,
        }, null);
    }
}

// ============================================================================
// Buffers
// ============================================================================

fn createBuffer(size: vk.DeviceSize, usage: vk.BufferUsageFlags, flags: vk.MemoryPropertyFlags) !VBuffer {
    const buffer_info = vk.BufferCreateInfo{
        .flags = .{},
        .size = size,
        .usage = usage,
        .sharing_mode = .exclusive,
        .queue_family_index_count = 0,
        .p_queue_family_indices = undefined,
    };

    const buffer = try vkd.createBuffer(device, buffer_info, null);
    const memory_requirements = vkd.getBufferMemoryRequirements(device, buffer);

    var memory_type: u32 = try findMemoryType(memory_requirements.memory_type_bits, flags);

    const vertex_buffer_memory = try vkd.allocateMemory(device, .{
        .allocation_size = memory_requirements.size,
        .memory_type_index = memory_type,
    }, null);

    try vkd.bindBufferMemory(device, buffer, vertex_buffer_memory, 0);

    return VBuffer{ .handle = buffer, .memory_handle = vertex_buffer_memory };
}

fn createVertexBuffer() !VBuffer {
    const buffer_size = @sizeOf(Vertex) * vertices.len;

    const staging_buffer = try createBuffer(
        buffer_size,
        .{ .transfer_src_bit = true },
        .{ .host_visible_bit = true, .host_coherent_bit = true },
    );

    var data = try vkd.mapMemory(device, staging_buffer.memory_handle, 0, buffer_size, .{});
    {
        @memcpy(@ptrCast([*]u8, data), @ptrCast([*]const u8, &vertices[0]), buffer_size);
    }
    vkd.unmapMemory(device, staging_buffer.memory_handle);

    const buffer = try createBuffer(
        buffer_size,
        .{ .vertex_buffer_bit = true, .transfer_dst_bit = true },
        .{ .device_local_bit = true },
    );

    try copyBuffer(staging_buffer.handle, buffer.handle, buffer_size);
    vkd.destroyBuffer(device, staging_buffer.handle, null);
    vkd.freeMemory(device, staging_buffer.memory_handle, null);

    return buffer;
}

fn createIndexBuffer() !VBuffer {
    const buffer_size = @sizeOf(u16) * indices.len;

    const staging_buffer = try createBuffer(
        buffer_size,
        .{ .transfer_src_bit = true },
        .{ .host_visible_bit = true, .host_coherent_bit = true },
    );

    var data = try vkd.mapMemory(device, staging_buffer.memory_handle, 0, buffer_size, .{});
    {
        @memcpy(@ptrCast([*]u8, data), @ptrCast([*]const u8, &indices[0]), buffer_size);
    }
    vkd.unmapMemory(device, staging_buffer.memory_handle);

    const buffer = try createBuffer(
        buffer_size,
        .{ .index_buffer_bit = true, .transfer_dst_bit = true },
        .{ .device_local_bit = true },
    );

    try copyBuffer(staging_buffer.handle, buffer.handle, buffer_size);
    vkd.destroyBuffer(device, staging_buffer.handle, null);
    vkd.freeMemory(device, staging_buffer.memory_handle, null);

    return buffer;
}

fn copyBuffer(src: vk.Buffer, dst: vk.Buffer, size: vk.DeviceSize) !void {
    const cb = try beginSingleTimeCommands();

    const copy_region = vk.BufferCopy{
        .src_offset = 0,
        .dst_offset = 0,
        .size = size,
    };
    vkd.cmdCopyBuffer(cb, src, dst, 1, @ptrCast([*]const vk.BufferCopy, &copy_region));

    try endSingleTimeCommands(cb);
}

// Create command buffers
fn createCommandBuffers() !void {
    try vkd.allocateCommandBuffers(device, .{
        .command_pool = command_pool,
        .level = .primary,
        .command_buffer_count = 1,
    }, @ptrCast([*]vk.CommandBuffer, &draw_cmd_buffer));

    try vkd.beginCommandBuffer(draw_cmd_buffer, .{
        .flags = .{},
        .p_inheritance_info = null,
    });
}

fn beginSingleTimeCommands() !vk.CommandBuffer {
    var cb: vk.CommandBuffer = undefined;
    try vkd.allocateCommandBuffers(device, .{
        .level = .primary,
        .command_pool = temp_command_pool,
        .command_buffer_count = 1,
    }, @ptrCast([*]vk.CommandBuffer, &cb));

    try vkd.beginCommandBuffer(cb, .{ .flags = .{ .one_time_submit_bit = true }, .p_inheritance_info = undefined });
    return cb;
}

fn endSingleTimeCommands(cb: vk.CommandBuffer) !void {
    try vkd.endCommandBuffer(cb);

    const submit_info = vk.SubmitInfo{
        .command_buffer_count = 1,
        .p_command_buffers = @ptrCast([*]const vk.CommandBuffer, &cb),
        .wait_semaphore_count = 0,
        .p_wait_semaphores = undefined,
        .p_wait_dst_stage_mask = undefined,
        .signal_semaphore_count = 0,
        .p_signal_semaphores = undefined,
    };

    try vkd.queueSubmit(graphics_queue, 1, @ptrCast([*]const vk.SubmitInfo, &submit_info), .null_handle);
    try vkd.queueWaitIdle(graphics_queue);

    vkd.freeCommandBuffers(device, temp_command_pool, 1, @ptrCast([*]const vk.CommandBuffer, &cb));
}

// { // Create Uniform buffer
//     const buffer_size: vk.DeviceSize = @sizeOf(UniformBufferObject);
//     uniform_buffers = try al.alloc(VBuffer, swapchain_images.len);

//     for (uniform_buffers) |*ub| {
//         ub.* = try createBuffer(buffer_size, .{ .uniform_buffer_bit = true }, .{ .host_visible_bit = true, .host_coherent_bit = true });
//     }
// }

fn createDescriptorPool() !void { // create Description Pool
    const pool_size = vk.DescriptorPoolSize{
        .@"type" = .combined_image_sampler,
        .descriptor_count = @intCast(u32, max_texture_count),
    };

    descriptor_pool = try vkd.createDescriptorPool(device, .{
        .flags = .{},
        .pool_size_count = 1,
        .p_pool_sizes = @ptrCast([*]const vk.DescriptorPoolSize, &pool_size),
        .max_sets = max_texture_count,
    }, null);
}

fn createTextureDescriptorSets(al: *Allocator) !void {

    // Create Descriptor sets
    var layouts = try al.alloc(vk.DescriptorSetLayout, max_texture_count);
    for (layouts) |*l| l.* = texture_descriptor_set_layout;
    defer al.free(layouts);

    const alloc_info = vk.DescriptorSetAllocateInfo{
        .descriptor_pool = descriptor_pool,
        .descriptor_set_count = max_texture_count,
        .p_set_layouts = layouts.ptr,
    };

    descriptor_sets = try al.alloc(vk.DescriptorSet, max_texture_count);
    _ = try vkd.allocateDescriptorSets(device, alloc_info, descriptor_sets.ptr);

    updateSampler(descriptor_sets[0], potato_image_view, 1);
    updateSampler(descriptor_sets[1], potato2_image_view, 1);
}

fn updateSampler(ds: vk.DescriptorSet, image_view: vk.ImageView, dst_binding: u32) void {
    const image_info = vk.DescriptorImageInfo{
        .image_layout = .shader_read_only_optimal,
        .image_view = image_view,
        .sampler = texture_sampler,
    };

    const descriptor_write = vk.WriteDescriptorSet{
        .dst_set = ds,
        .dst_binding = dst_binding,
        .dst_array_element = 0,
        .descriptor_type = .combined_image_sampler,
        .descriptor_count = 1,
        .p_buffer_info = undefined,
        .p_image_info = @ptrCast([*]const vk.DescriptorImageInfo, &image_info),
        .p_texel_buffer_view = undefined,
    };

    vkd.updateDescriptorSets(
        device,
        1,
        @ptrCast([*]const vk.WriteDescriptorSet, &descriptor_write),
        0,
        undefined,
    );
}
